<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/diemdanh', 'HomeController@diemDanh')->name('diemdanh');
Route::get('charts','ChartController@index');

Route::group(['prefix' => 'admin'], function() {
    Route::get('admin-page', ['as'=>'admin.index','uses'=>'AdminController@index']);
    Route::get('taotaikhoan', ['as'=>'admin.taotaikhoan','uses'=>'AdminController@taoTaiKhoan']);
});
Route::group(['prefix' => 'don'], function() {
    Route::get('danhsach', ['as'=>'don.danhsach','uses'=>'DonController@danhSach']);
    Route::get('ditrevesom', ['as'=>'don.ditrevesom','uses'=>'DonController@diTreVeSom']);
    Route::get('ditrevesom-print', ['as'=>'don.ditrevesom-print','uses'=>'DonController@diTreVeSomPrint']);
});
