@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @if (true)
                        <p>
                            Bạn đã điểm danh
                        </p>
                    @else
                        <p>
                            Bạn chưa đã điểm danh
                        </p>
                    @endif
                </div>

                <div class="panel-body">
                    @if (true)
                        <button type="button" class="btn btn-block btn-danger btn-lg">Rời đi</button>
                    @else
                        <button type="button" class="btn btn-block btn-primary btn-lg">Điểm danh</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
