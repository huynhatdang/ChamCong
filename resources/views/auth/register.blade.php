@extends('layouts.app')

@section('content')

<div class="container register">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">
            <div class="row">
            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {!! csrf_field() !!}
                <div class="col-md-6">
                <div class="aa-myaccount-login">
                    <h4>Thông tin tài khoản</h4>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Tài khoản</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">E-Mail</label>

                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Mật khẩu</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Xác nhận mật khẩu</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation">

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-md-6">
                <div class="aa-myaccount-register">
                 <h4>Thông tin nhân viên</h4>
                    <div class="form-group{{ $errors->has('txtname') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label">Tên nhân viên</label>

                        <div class="col-md-6">
                            <input type="text" class="form-control" name="txtname" value="{{ old('txtname') }}">

                            @if ($errors->has('txtname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('txtname') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('txtphone') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label">Số điện thoại</label>

                        <div class="col-md-6">
                            <input type="text" class="form-control" name="txtphone" value="{{ old('txtphone') }}">

                            @if ($errors->has('txtphone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('txtphone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('txtadr') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label">Địa chỉ</label>

                        <div class="col-md-6">
                        <input type="text" class="form-control" name="txtadr" value="{{ old('txtadr') }}">

                            @if ($errors->has('txtadr'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('txtadr') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('slRole') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label">Chọn quyền</label>

                        <div class="col-md-6">
                            <select class="form-control custom-select" name="slRole"  value="{{ old('slRole') }}">
                                <option selected>Vui lòng chọn quyền</option>
                                <option value="1">Admin</option>
                                <option value="2">Quản trị viên</option>
                                <option value="3">Báo cáo thống kê</option>
                                <option value="4">Nhân viên</option>
                            </select>
                        <!-- <input type="text" class="form-control" name="txtadr" value="{{ old('slRole') }}"> -->

                            @if ($errors->has('slRole'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('slRole') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-5">
                    <button type="submit button" class="btn btn-primary">Đăng ký</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
   </div>
@endsection
