@extends('layouts.app')

@section('content')
<section class="content-header">
  <h1>
    Danh sách đơn đã nộp
    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Mẫu Đơn</a></li>
    <li><a href="#">Danh sách đơn đã nộp</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-body">
          <table id="danhSachDonDaNop" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="danhSachDonDaNop_info" > 
            <thead>
            <tr>
              <th>Loại đơn</th>
              <th>Ngày nộp</th>
              <th>Trạng thái</th>
              <th>In</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>Đi trễ-về sớm</td>
              <td>16/12/2017 8:00</td>
              <td>Đang chờ duyệt</td>
              <td>
                <a href="ditrevesom-print" target="_blank" class="btn btn-default">
                  <i class="fa fa-print"></i> In
                </a>
              </td>
            </tr>
            <tr>
              <td>Đơn xin nghỉ</td>
              <td>14/12/2017 08:00</td>
              <td>Đã duyệt</td>
              <td>
                <a href="ditrevesom-print" target="_blank" class="btn btn-default">
                  <i class="fa fa-print"></i> In
                </a>
              </td>
            </tr>
            <tr>
              <td>Biên bản bàn giao</td>
              <td>15/12/2017 08:00</td>
              <td>Đang chờ duyệt</td>
              <td>
                <a href="ditrevesom-print" target="_blank" class="btn btn-default">
                  <i class="fa fa-print"></i> In
                </a>
              </td>
            </tr>
            <tr>
              <td>Đơn xin thôi việc
              <td>16/12/2017 16:00</td>
              <td>Từ chối</td>
              <td>
                <a href="ditrevesom-print" target="_blank" class="btn btn-default">
                  <i class="fa fa-print"></i> In
                </a>
              </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
