@extends('layouts.app')

@section('content')
<div class="container">
  <div class="col-md-11">
    <!-- Horizontal Form -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Đơn xin đi trễ về sớm</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form class="form-horizontal">
        <div class="box-body">

          <div class="form-group">
            <label for="inputLyDo" class="col-sm-2 control-label">Lý do</label>
            <div class="col-sm-10">
              <form>
                <textarea class="textarea" placeholder="Điền lý do xin phép đi trễ-về sớm vào đây"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </form>
            </div>
          </div>

          <div class="form-group">
            <label for="inputLyDo" class="col-sm-2 control-label">Lý do</label>

            <div class="col-sm-10">
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right" id="reservation">
              </div>
            </div>
          </div>


          <div class="form-group">
            <label for="inputLyDo" class="col-sm-2 control-label">Ghi chú</label>
            <div class="col-sm-10">
              <form>
                <textarea class="textarea" placeholder="Ghi chú"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </form>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button type="submit" class="btn btn-info pull-right">Nộp đơn</button>
        </div>
        <!-- /.box-footer -->
      </form>
    </div>
  </div>
</div>
@endsection
