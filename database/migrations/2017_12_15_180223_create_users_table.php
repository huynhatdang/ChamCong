<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name',15);
			$table->string('email',30)->unique();
			$table->string('password', 250);
			$table->boolean('confirmed')->default(0);
			$table->string('token', 254)->nullable();
			$table->integer('loainguoidung_id')->unsigned();
            $table->foreign('loainguoidung_id')->references('id')->on('loainguoidung')->onUpdate('cascade');
			$table->rememberToken();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
