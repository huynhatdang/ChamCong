<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\NhanVien;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'name' =>'required|unique:users,name',
            'email' =>'required|unique:users,email|regex:^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})^',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' =>'required|same:password',
            'txtname' =>'required',
            'txtphone' =>'required|numeric|digits_between:10,12',
            'txtadr' =>'required',
        ];

        $messages = [
            'required'=> 'Vui lòng không để trống trường này!',
            'name.unique'   =>'Dữ liệu này đã tồn tại!',
            'email.unique'  =>'Dữ liệu này đã tồn tại!',
            'email.regex'  =>'Email không đúng định dạng!',
            'password_confirmation.same' =>'Mật khẩu không trùng khớp!',
            'digits_between'=>'Số điện thoại phải từ 10 đến 12 số!'
        ];

        return Validator::make($data,$rules,$messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user= User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'loainguoidung_id' => 2,
            'token'=>Str::random(40)
        ])->toArray();

        NhanVien::create([
            'nhanvien_ten' => $data['txtname'],
            'nhanvien_email' => $data['email'],
            'nhanvien_sdt' => $data['txtphone'],
            'nhanvien_dia_chi' => $data['txtadr'],
            'user_id' => $user['id']
        ]);

        return $user;
    }
}
