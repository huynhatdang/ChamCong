<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class DonController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function danhSach()
    {
        return view('don.danhsach');
    }
    public function diTreVeSom()
    {
        return view('don.ditrevesom');
    }
    public function diTreVeSomPrint()
    {
        return File::get(public_path() . '/print/don/ditrevesom-print.html');
        // return view('don.ditrevesom-print');
    }
}
