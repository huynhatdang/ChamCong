<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NhanVien extends Model
{
    protected $table = "nhanvien";

    protected $fillable = ['nhanvien_ten','nhanvien_email','nhanvien_sdt','nhanvien_dia_chi','user_id'];

	public $timestamps = false;
}
