<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','loainguoidung_id','confirmed','token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isOwner()
    {
        return ($this->loainguoidung_id == 1);

    }
    public function isAdmin()
    {
        return ($this->loainguoidung_id == 2);
    }
    public function isKho()
    {
        return ($this->loainguoidung_id == 3);
    }
    public function isMember()
    {
        return ($this->loainguoidung_id == 4);
    }
}
